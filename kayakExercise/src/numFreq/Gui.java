/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numFreq;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Gui extends Application {

  @Override
  public void start(Stage primaryStage) throws Exception {

    BorderPane mainPane = new BorderPane();
    GridPane grid = new GridPane();
    grid.setAlignment(Pos.CENTER);
    grid.setHgap(5);
    grid.setVgap(5);

    TextField inputData = new TextField();
    Label inputLabel = new Label("Enter integer numbers separated by a comma:");
    Label errorLabel = new Label();
    Label display = new Label();
    Label empty = new Label();
    RadioButton txtOption = new RadioButton("numeric");
    txtOption.setUserData(true);

    RadioButton graphOption = new RadioButton("graphic");
    graphOption.setUserData(false);

    ToggleGroup grp = new ToggleGroup();
    txtOption.setToggleGroup(grp);
    txtOption.setSelected(true);
    graphOption.setToggleGroup(grp);

    Button calcBtn = new Button("Show Frequencies");

    ScrollPane scrollDisplay = new ScrollPane();
    scrollDisplay.setContent(display);
    calcBtn.setOnAction(event -> {
      Integer[] numbers = null;

      if (!inputData.getText().isEmpty()) {
        numbers = parseInputField(inputData.getText());
      }
      if (numbers != null) {

        if ((boolean) grp.getSelectedToggle().getUserData()) {
          display.setText(NumberFrequency.generateResult(NumberFrequency.countFrequency(numbers), true));
          errorLabel.setText("");
        } else {
          display.setText(NumberFrequency.generateResult(NumberFrequency.countFrequency(numbers), false));
          errorLabel.setText("");
        }

      } else {
        errorLabel.setText("You can only enter integer numbers!");
      }


    });

    display.setFont(Font.font("Monospaced", 20));
    grid.add(errorLabel, 0, 0);
    grid.add(calcBtn, 0, 4);
    grid.add(inputLabel, 0, 2);
    grid.add(inputData, 0, 3);
    grid.add(txtOption, 1, 3);
    grid.add(graphOption, 1, 4);
    mainPane.setCenter(scrollDisplay);
    mainPane.setBottom(grid);
    mainPane.setTop(empty);
    // grid.setGridLinesVisible(true);
    Scene scene = new Scene(mainPane, 400, 400);

    //primaryStage.setResizable(false);
    primaryStage.setScene(scene);
    primaryStage.setTitle("Number Frequency");
    primaryStage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }


  // validates and parses input field
  private static Integer[] parseInputField(String inputText) {
    String inp = inputText.replaceAll("\\s", "");
    String[] numbers = inp.split(",");
    Integer[] parsedNumbers = new Integer[numbers.length];

    Pattern illegalChars = Pattern.compile("([^\\d-]*)");

    for (int i = 0; i < numbers.length; ++i) {
      Matcher match = illegalChars.matcher(numbers[i]);
      while (match.find()) {
        if (!match.group().isEmpty()) {
          return null;
        }
      }
      try {
        parsedNumbers[i] = Integer.parseInt(numbers[i]);
      } catch (NumberFormatException e) {
        return null;
      }
    }

    return parsedNumbers;
  }

}

