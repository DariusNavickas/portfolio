package numFreq;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <h1>Number Frequency</h1>
 * Number Frequency program counts the frequency of occurence of integer numbers
 * in a given array and prints out either numeric or graphic representation of
 * number and frequency of occurence
 *
 * @author Darius Navickas
 * @version 1.0
 * @since 2018-10-31
 */
public class NumberFrequency {

    private static Integer max;
    private static Integer min;

    /**
     * countFrequency method takes a given array of Integer numbers and counts
     * the frequency of occurence of each number and returns a Map of the
     * numbers and their corresponding frequencies. It also finds the minimum
     * and maximum numbers of an array. If a number between the range of min and
     * max is not present, it is then added to the result with corresponding
     * frequence of zero.
     *
     * @param list This is an array of Integer numbers
     * @return Map{@literal <}Integer,Integer{@literal >} returns a tree map
     * containing all the numbers and their corresponding frequencies
     *
     */
    public static Map<Integer, Integer> countFrequency(Integer[] list) {
        //count frequency
        Map<Integer, Integer> mapFreq = Arrays.stream(list)
                .collect(Collectors.toMap(x -> x, x -> 1, Integer::sum));

        List<Integer> numbers = new ArrayList<>(mapFreq.keySet());
        max = numbers.stream().reduce(Integer.MIN_VALUE, Integer::max);
        min = numbers.stream().reduce(Integer.MAX_VALUE, Integer::min);

        //Map<Number,Frequency>
        Map<Integer, Integer> result = new TreeMap<>();
        for (int i = min; i <= max; ++i) {
            if (mapFreq.get(i) != null) {
                result.put(i, mapFreq.get(i));
            } else {
                result.put(i, 0);
            }
        }
        return result;
    }

    /**
     * generateResult method takes a Map{@literal <}Integer,Integer{@literal >}
     * as a data source and depending on type variable generates either
     * graphical or numerical representation of integer numbers and its
     * frequencies from the data variable
     *
     * @param data This is a data variable of type
     * Map{@literal <}Integer,Integer{@literal >} where key is the number and a
     * value is its frequency
     * @param type Type variable is a boolean variable which determines what
     * kind of representation will be generated, true for Numeric, false for
     * graphic representation.
     * @return Returns a generated string of a chosen representation
     */
//   Map<number, frequency>
    public static String generateResult(Map<Integer, Integer> data, boolean type) {
        StringBuilder buildGraph = new StringBuilder();
        if (type) {
            // print numeric
            buildGraph.append("frequency:");
            for (Integer k : data.keySet()) {
                buildGraph.append(k < 0 ? " " + data.get(k) + setNumericAlignment(k, data.get(k)) : data.get(k) + setNumericAlignment(k, data.get(k)) + " ");
            }
            buildGraph.append("\n");
            buildGraph.append("numbers  :");
            for (int j = min; j <= max; ++j) {
                buildGraph.append(j + " ");
            }
        } else {
            // print graph
            Integer maxFreq = data.values().stream().reduce(Integer.MIN_VALUE, Integer::max);
            for (int i = maxFreq; i > 0; i--) {
                for (Integer j : data.keySet()) {
                    buildGraph.append(setGraphicAlignment(data.get(j), j, i));
                }
                buildGraph.append("\n");
            }
            for (int j = min; j <= max; ++j) {
                buildGraph.append(j < 0 ? j + "  " : " " + j + " ");
            }
        }
        return buildGraph.toString();
    }

    /**
     * setGraphicAlignment method is build specifically for alignment of
     * graphical representation of frequencies and numbers of an array. It is
     * not meant to be used in places other than graphical representation part
     * of generateResult method This method determines how many spaces is going
     * to be needed for each number to be aligned with a graphical
     * representation of its frequency
     *
     * @param frequency This is a frequency of a given number
     * @param key This is a key value of a
     * Map{@literal <}Integer,Integer{@literal >} where key value is a number
     * from an array
     * @param index This is a value of a current loop iteration from
     * generateResult method
     * @return String Returns a string that is correctly aligned for a specific
     * number
     */
    private static String setGraphicAlignment(Integer frequency, Integer key, Integer index) {
        String display = "";
        if (frequency != null && index != null) {
            if (index <= frequency) {
                display += " * ";
                for (int i = 0; i < key.toString().length() - 1; ++i) {
                    display += " ";
                }
            } else {
                display += "   ";
                for (int i = 0; i < key.toString().length() - 1; ++i) {
                    display += " ";
                }
            }
        }
        return display;
    }

    /**
     * setNumericAlignment method is build specifically for a numeric
     * representation of numbers and frequencies alignment. It is not meant to
     * be used in places other than numeric representation part of
     * generateResult method. This method determines how many spaces is going to
     * be needed for each frequency number to be aligned with an actual number.
     * I wonder if you guys will read all of this information :)
     *
     * @param number This is a number to which a numeric frequency
     * representation will be aligned
     * @param frequency This is a value of a frequency of occurrence of a number
     * param.
     * @return String Returns a correct amount of white-space needed for correct
     * aligment of numbers and frequencies
     */
    private static String setNumericAlignment(Integer number, Integer frequency) {
        StringBuilder alignment = new StringBuilder();
        for (int i = 0; i < number.toString().length(); ++i) {
            alignment.append(" ");
        }
        for (int j = 0; j < frequency.toString().length(); ++j) {
            alignment.deleteCharAt(alignment.toString().length() - 1);
        }
        return alignment.toString();
    }
}
