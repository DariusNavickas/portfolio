/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit.dynamicTable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gebruiker
 */
public class DataBase {

    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private final String DB_URL = "jdbc:mysql://localhost:3306/employees?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private final String USER = "root";
    private final String PASS = "admin";
    private Connection connection;
    static {try {
        Class.forName(JDBC_DRIVER);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
}
    private Statement statement;
    private PreparedStatement pStatement;

    public void openConnection() throws ClassNotFoundException, SQLException {
        
        this.connection = DriverManager.getConnection(DB_URL, USER, PASS);

    }

    public void createStatement() throws SQLException {
        this.statement = connection.createStatement();
    }

    public void prepareStatement(String statement) throws SQLException {
        this.pStatement = connection.prepareStatement(statement);
    }

    public void setPrepParams(int number, String param) throws SQLException {
        this.pStatement.setString(number, param);
    }

    public ResultSet execPStatement() throws SQLException {
        return this.pStatement.executeQuery();
    }

    public void closeConnection() throws SQLException {
        connection.close();
    }

    public void closeStatement() throws SQLException {
        statement.close();
    }

    public Connection getConnection() {
        return connection;
    }

    public Statement getStatement() {
        return statement;
    }

    public String getJDBC_DRIVER() {
        return JDBC_DRIVER;
    }

    public String getDB_URL() {
        return DB_URL;
    }

    public String getUSER() {
        return USER;
    }

    public String getPASS() {
        return PASS;
    }

}
