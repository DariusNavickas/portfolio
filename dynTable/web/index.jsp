<%-- 
    Document   : index
    Created on : Sep 28, 2018, 11:59:55 AM
    Author     : Gebruiker
--%>

<%@page import="java.util.Collections"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <title>Dynamic table</title>
    </head>
    <jsp:include page="GetTableInfo" flush="true" />
    <body style="background: #D59A1F">

        <% ArrayList<String> names = (ArrayList<String>) request.getAttribute("TABLE_NAMES"); %>
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <form class="btn-group " action="RetrieveTable" method="post">
                        <% for (int i = 0; i < names.size(); ++i) {%>
                        <input class="btn btn-warning flex-fill font-weight-bold text-uppercase btn-lg" type="submit" name="table_name" value=<%=names.get(i)%>  >
                        <%
                            }

                        %>

                    </form>
                </div>
            </div>
            <%  ArrayList<String> columnNames = new ArrayList();
                ArrayList<String> columnData = new ArrayList();
                if (request.getAttribute("COLUMN_NAMES") != null && request.getAttribute("COLUMN_DATA") != null) {
                    columnNames = (ArrayList<String>) request.getAttribute("COLUMN_NAMES");
                    int cc = 0;
                    columnData = (ArrayList<String>) request.getAttribute("COLUMN_DATA");
                    String tableName = (String) request.getAttribute("TABLE_NAME");
                    if (tableName.isEmpty()|| !tableName.equals(null)) {
                        request.getSession().setAttribute("TABLE_NAME", tableName); 
            %>
                        
            <form action="Filter" method="post" class="form-group">
                <div class="form-group d-flex flex-row">
                    
                 <%for(int i =0; i< columnNames.size();++i){ %>
                 
                 <input type="text" placeholder="<%=columnNames.get(i) %>" name="<%=columnNames.get(i).toUpperCase() %>" class="form-control" >
                 <% } %>
                 <% request.getSession().setAttribute("COLUMN_NAMES", columnNames);  %>
                <input type="submit" name="filter" value="FILTER" class="btn btn-warning">
                </div>
            </form>
            <%  }

            %> 
            <div class="row">
                <div class="col-12 d-flex flex-column justify-content-center ">
                    <h1 class="text-capitalize">  <%= request.getAttribute("TABLE_NAME")%> </h1>
                    <table class="table table-striped bg-warning"> 
                        <thead class="thead-dark">
                            <%for (int l = 0; l < columnNames.size(); l++) {%>
                        <th class="text-capitalize"><%=columnNames.get(l)%> </th>
                            <%} %>
                        </thead>

                        <% for (int k = 0; k < columnData.size(); k += columnNames.size()) { %>
                        <tr> <% for (int j = 0; j < columnNames.size(); j++) {%>
                            <td> <%= columnData.get(cc++)%> </td>
                            <% } %>
                        </tr>
                        <% } %>    
                    </table>
                </div>
            </div>
        </div>

        <% }%>




    </body>
</html>
