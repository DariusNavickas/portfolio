/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit.sklepas;

import entityWrappers.CartInfoWrapper;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import lt.bit.sklepas.entities.CartInfo;
import lt.bit.sklepas.entities.Product;
import lt.bit.sklepas.entities.ProductInfo;
import lt.bit.sklepas.entities.ShoppingCart;
import lt.bit.sklepas.entities.Users;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Gebruiker
 */
@Path("ShoppingController")
public class ShoppingController extends HttpServlet {

    private final Log logger = LogFactory.getLog(ShoppingController.class);

    @Path("AddToShoppingCart/{id}")
    @POST
    public String addToShoppingCart(@Context HttpServletRequest request, @PathParam("id") Integer productId) {
        HttpSession session = request.getSession(false);
        logger.info("suveike addToShoppingCart");
        // shopping carto nera
        if (session.getAttribute("shoppingCartId") == null) {
            logger.info("shoppingCartID yra null");
            EntityManager mgr = null;
            ShoppingCart cart = null;
            // jei neprisilogines sukuriam nauja shopping carta
            if (session.getAttribute("userId") == null) {
                logger.info("Creating new shopping cart");
                cart = new ShoppingCart();
                cart.setDateOfCreation(new Date());
            } else {
                try {
                    //jei prisijunges naudojam userio shoping carta
                    logger.info("getting users shopping cart");
                    mgr = EMF.getEntityManager();
                    Users user = mgr.find(Users.class, session.getAttribute("userId"));
                    Query shoppingCartQuery = mgr.createQuery("Select sc from ShoppingCart sc where user_id=" + user.getUserId());
                    cart = (ShoppingCart) shoppingCartQuery.getSingleResult();
                    logger.info(cart.toString());
                } catch (Exception e) {
                    logger.error("threw an error " + e.getMessage());
                    e.printStackTrace();
                } finally {
                    EMF.returnEntityManager(mgr);
                }

            }

            try {
                mgr = EMF.getEntityManager();
                EntityTransaction tx = EMF.getTransaction(mgr);

                logger.info("persistinam carta");
                Product product = mgr.find(Product.class, productId);
                CartInfo cartInfo = new CartInfo();
                cartInfo.setCartId(cart);
                cartInfo.setProductId(product);
                cartInfo.setQuantity(1);
                cartInfo.setPrice(product.getPrice());
                //   logger.info("cartas :" + cart.toString());
                logger.info("cartInfo :" + cartInfo.toString());
                if (session.getAttribute("userId") == null) {
                    mgr.persist(cart);
                } else {
                    mgr.merge(cart);
                }
                mgr.persist(cartInfo);
                logger.info("persistinam cart info");
                EMF.commitTransaction(tx);

            } catch (Exception e) {
                logger.error("threw an exception " + e.getMessage());
                e.printStackTrace();
            } finally {
                EMF.returnEntityManager(mgr);
                logger.info("entity manager returned");
            }
            session.setAttribute("shoppingCartId", cart.getCartId());

        } else {
            // neprisijunges bet shoping cartas jau yra
            logger.info("shopping cartas jau yra");

            Integer shoppingCartId = (Integer) session.getAttribute("shoppingCartId");
            //  ShoppingCart cart = (ShoppingCart) session.getAttribute("shopppingCart");
            logger.info(shoppingCartId);
            EntityManager mgr = null;
            try {
                mgr = EMF.getEntityManager();
                ShoppingCart cart = mgr.find(ShoppingCart.class, shoppingCartId);
                Product product = mgr.find(Product.class, productId);
                Query query = mgr.createQuery("Select ci from CartInfo ci where cart_id=" + cart.getCartId() + "and product_id =" + productId);
                try {
                    logger.info("updating current CartInfo");
                    logger.info("current cartID: " + cart.getCartId());
                    CartInfo singleResult = (CartInfo) query.getSingleResult();
                    Integer currentQuantity = singleResult.getQuantity();
                    BigDecimal currentPrice = singleResult.getPrice();
                    singleResult.setPrice(currentPrice.add(product.getPrice()));
                    singleResult.setQuantity(currentQuantity + 1);
                    EntityTransaction tx = EMF.getTransaction(mgr);
                    EMF.commitTransaction(tx);
                } catch (NoResultException e) {
                    // jei cartas tuscias
                    logger.error("threw an exception " + e.getMessage());
                    logger.info("creating new Cartinfo");
                    CartInfo cartInfo = new CartInfo();
                    cartInfo.setCartId(cart);
                    cartInfo.setProductId(product);
                    cartInfo.setQuantity(1);
                    cartInfo.setPrice(product.getPrice());
                    EntityTransaction tx = EMF.getTransaction(mgr);
                    mgr.persist(cartInfo);
                    EMF.commitTransaction(tx);
                }
            } catch (Exception e) {
                logger.error("threw an error " + e.getMessage());
                e.printStackTrace();
            } finally {
                EMF.returnEntityManager(mgr);
                logger.info("entity manager returned");
            }

        }
        return null;
    }

    @Path("GetShoppingCartData")
    @POST
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public List<CartInfoWrapper> getShoppingCartData(@Context HttpServletRequest request) {
        HttpSession session = request.getSession();
        EntityManager mgr = null;
        if (session.getAttribute("shoppingCartId") != null) {
            try {
                Integer cartId = (Integer) session.getAttribute("shoppingCartId");
                mgr = EMF.getEntityManager();
                ShoppingCart shoppingCart = mgr.find(ShoppingCart.class, cartId);
                Collection<CartInfo> cartInfoCollection = shoppingCart.getCartInfoCollection();
                List<CartInfoWrapper> wrappedCartInfoCollection = new ArrayList();
                for (CartInfo ci : cartInfoCollection) {
                    Query productQ = mgr.createQuery("Select pi from ProductInfo pi where product_id =" + ci.getProductId().getProductId());
                    ProductInfo productInfo = (ProductInfo) productQ.getSingleResult();
                    logger.info(productInfo.getName());
                    wrappedCartInfoCollection.add(new CartInfoWrapper(ci, productInfo.getName()));
                }
                logger.info(cartInfoCollection.toString());
                return wrappedCartInfoCollection;
            } catch (Exception e) {
                logger.error("threw an error " + e.getMessage());
                e.printStackTrace();
            } finally {
                EMF.returnEntityManager(mgr);
            }
        } else {
            logger.info("shoppingCartId yra null");
        }
        return null;
    }

    @Path("RemoveShoppingCartInfo/{id}")
    @POST
    public void removeShoppingCartInfo(@PathParam("id") Integer id) {
        EntityManager mgr = null;
        if (id != null) {
            try {
                mgr = EMF.getEntityManager();
                EntityTransaction tx = EMF.getTransaction(mgr);
                CartInfo cartInfo = mgr.find(CartInfo.class, id);
                mgr.remove(cartInfo);
                EMF.commitTransaction(tx);
            } catch (Exception e) {
                logger.error("threw an exception " + e.getMessage());
                e.printStackTrace();
            } finally {
                logger.info("returned entity manager");
                EMF.returnEntityManager(mgr);
            }
        } else {
            logger.info("Cart info id is null");
        }

    }
//TODO make it so method returns new quantity and price data or maybe whole object and update fields in js on success instead of reload
    @Path("ChangeQuantity/{id}")
    @POST
    public void changeCartInfoQuantity(@PathParam("id") Integer id, @QueryParam("quantity") Integer quantity) {
        EntityManager mgr = null;
        if (id != null && quantity != null) {
            try {
                mgr = EMF.getEntityManager();
                EntityTransaction tx = EMF.getTransaction(mgr);
                CartInfo cartInfo = mgr.find(CartInfo.class, id);
                Product product = cartInfo.getProductId();
                BigDecimal multiply = BigDecimal.valueOf(Double.valueOf(quantity.toString()));
                
                cartInfo.setQuantity(quantity);
                cartInfo.setPrice(product.getPrice().multiply(multiply));
                logger.info("changing quantity to "+ quantity);
                logger.info("changing price to "+ cartInfo.getPrice());
                EMF.commitTransaction(tx);

            } catch (Exception e) {
                logger.error("threw an exception " + e.getMessage());
                e.printStackTrace();
            } finally {
                EMF.returnEntityManager(mgr);
            }
        } else {
            logger.info("Cart info id or quantity  is null");
        }

    }

}
