/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit.sklepas;

import java.io.IOException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import lt.bit.sklepas.entities.CartInfo;
import lt.bit.sklepas.entities.ShoppingCart;
import lt.bit.sklepas.entities.Users;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Gebruiker
 */
@Path("LoginValidation")
public class LoginValidation extends HttpServlet {

    public final Log logger = LogFactory.getLog(LoginValidation.class);

    /**
     *
     * @param user
     * @return
     */
    @POST
    @Path("Validate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_HTML)
    public String validate(Users user, @Context HttpServletRequest request) throws IOException, Exception {
        EntityManager mgr = EMF.getEntityManager();
        Query qry = mgr.createNamedQuery("Users.findByUserName");
        qry.setParameter("userName", user.getUserName());
        Users resultUser = null;
        try {
            resultUser = (Users) qry.getSingleResult();
            if (resultUser != null) {
                String salt = resultUser.getSalt();
                String password = resultUser.getPassword();
                logger.info(resultUser.getPassword());
                logger.info(HashGenerator.makeHash(user.getPassword(), salt));
                logger.info("success");
                //verify password
                if (password.equals(HashGenerator.makeHash(user.getPassword(), salt))) {
                    logger.info(password);
                    logger.info(HashGenerator.makeHash(user.getPassword(), salt));
                    logger.info(resultUser.getUserType()+ " -===============================================================================");
                    // create session and set user Type
                    HttpSession session = request.getSession(true);
                    session.setAttribute("userType", resultUser.getUserType());
                    session.setAttribute("loggedIn", (short) 0);
                    session.setAttribute("userName", resultUser.getUserName());
                    session.setAttribute("userId", resultUser.getUserId());
                    
                    Query shoppingCartQuery = mgr.createQuery("Select sc from ShoppingCart sc where user_id ="+resultUser.getUserId());
                    ShoppingCart shoppingCart = (ShoppingCart) shoppingCartQuery.getSingleResult();
                    if(session.getAttribute("shoppingCartId") == null){
                     session.setAttribute("shoppingCartId", shoppingCart.getCartId());
                     logger.info("shoppingCartId is set");
                    }else{
                        // find all cart info rows where id is current cart and set it to users cart id
                        EntityTransaction tx = EMF.getTransaction(mgr);
                      logger.info("merging carts");
                        Query currentCartInfoQ = mgr.createQuery("Select ci from CartInfo ci where cart_id="+session.getAttribute("shoppingCartId"));
                        List<CartInfo> currentCartInfoList = currentCartInfoQ.getResultList();
                        for(CartInfo ci : currentCartInfoList){
                        ci.setCartId(shoppingCart);
                        }
                        // remove annonymous shopping cart
                        ShoppingCart annonShoppingCart = mgr.find(ShoppingCart.class, session.getAttribute("shoppingCartId"));
                        mgr.remove(annonShoppingCart);
                        EMF.commitTransaction(tx);
                        // set merged users shopping cart id to session
                        session.setAttribute("shoppingCartId", shoppingCart.getCartId());
                        logger.info("carts merged");
                    }
                   

                    return "success";
                }
            }
        } catch (NoResultException e) {
            logger.info("failure");
            return "User name not found";

        } finally {
            EMF.returnEntityManager(mgr);
            logger.info("entity manager returned");
        }
        return "failure";
    }

    @POST
    @Path("GetUserState")
    @Produces(MediaType.APPLICATION_JSON)
    public UserState getUserState(@Context HttpServletRequest request) {
       HttpSession session = request.getSession(false);
        try{
         session = request.getSession();
        if (session.getAttribute("userType") != null && session.getAttribute("loggedIn") !=null) {
            short userType = (short) session.getAttribute("userType");
            logger.info("getting user state");
            logger.info(userType);
            short loggedIn = (short) session.getAttribute("loggedIn");
            logger.info("is user logged in ?");
            logger.info(loggedIn);
            
            return new UserState(userType, loggedIn);
            
        }
        }catch(Exception e){
            logger.info("threw an exception "+ e.getMessage());
           e.printStackTrace();
        }
        

       

        return new UserState((short) 1 , (short) 1);
    }

    @POST
    @Path("Logout")
    public void logout(@Context HttpServletRequest request) {
     
        HttpSession session = request.getSession(false);
       if(session != null){
             session.invalidate();
         logger.info("sesssion Invalidated: " + session.toString());
       }
  
        

    }

}
