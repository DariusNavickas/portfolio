/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit.sklepas;

import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import lt.bit.sklepas.entities.ShoppingCart;
import lt.bit.sklepas.entities.Users;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Gebruiker
 */
@Path("Registration")
public class Register {

    public final Log logger = LogFactory.getLog(Register.class);

    @POST
    @Path("Register")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_HTML)
    public String register(Users newUser) throws Exception {
       
        EntityManager manager = EMF.getEntityManager();
        Query namedQuery = manager.createNamedQuery("Users.findByUserName");
        namedQuery.setParameter("userName", newUser.getUserName());
        manager.clear();
        EntityTransaction t = EMF.getTransaction(manager);
        try {
            Users checkForExisting = (Users) namedQuery.getSingleResult();
            if (checkForExisting != null) {
                logger.info("user exists");
                EMF.returnEntityManager(manager);
                return "User name already exists";
            }

        } catch (NoResultException e) {
            logger.info("Exception: " + e.getMessage());
            e.printStackTrace();
            logger.info("new user creation");
            String salt = HashGenerator.generateSalt(newUser.getPassword());
            String password = newUser.getPassword();
            newUser.setPassword(HashGenerator.makeHash(password, salt));
            newUser.setSalt(salt);
            short type = 1;
            newUser.setUserType(type);
            logger.info(newUser.toString());
            Users u = newUser;
            ShoppingCart newCart = new ShoppingCart();
            newCart.setUserId(u);
            newCart.setDateOfCreation(new Date());
            manager.persist(u);
            manager.persist(newCart);
            // manager.flush();
            logger.info("commiting transaction");
            EMF.commitTransaction(t);
            EMF.returnEntityManager(manager);
        }
        return "User successfully registered";
    }

}
