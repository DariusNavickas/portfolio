/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit.sklepas;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Gebruiker
 */
@WebFilter(filterName = "UserTypeFilter", urlPatterns = {"/adminPanel/*"})
public class UserTypeFilter implements Filter {

    public UserTypeFilter() {
    }

    private static final org.apache.commons.logging.Log logger = LogFactory.getLog(UserTypeFilter.class);

    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {

        HttpSession session = ((HttpServletRequest) request).getSession(false);
        if (null == session || null == session.getAttribute("userType")) {
            logger.info("nera sesijos arba atributo");
//            RequestDispatcher disp = ((HttpServletRequest) request).getRequestDispatcher("index.html");
//            disp.forward(request, response);

        } else if (session.getAttribute("userType") != null) {
            short type = (short) session.getAttribute("userType");
            if ((short) 0 == type) {
                logger.info("admino teises");
                 logger.info(session.getAttribute("userType"));
                chain.doFilter(request, response);
            }else{
            logger.info("vartotojas neturi admino teises");
            logger.info(session.getAttribute("userType"));
            }

        }

    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {

    }

}
