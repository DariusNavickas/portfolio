/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit.sklepas;

import com.mysql.cj.Session;
import entityWrappers.ProductInfoWrapper;
import entityWrappers.ProductWrapper;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import lt.bit.sklepas.entities.Categories;
import lt.bit.sklepas.entities.Product;
import lt.bit.sklepas.entities.ProductInfo;
import lt.bit.sklepas.entities.Restock;
import lt.bit.sklepas.entities.RestockInfo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Gebruiker
 */
@Path("AdminPanel")
public class AdminPanel {

    private final Log logger = LogFactory.getLog(AdminPanel.class);

    @POST
    @Path("getProductList")
    @Produces(MediaType.APPLICATION_JSON)
    public List getProductList(Integer categoryId) {
        List products = null;
        EntityManager mgr = null;
        try {
            mgr = EMF.getEntityManager();
            Query query = mgr.createQuery("Select p from ProductInfo p where categoryId = " + categoryId);
            products = query.getResultList();
        } catch (Exception e) {
            logger.error("getProductList threw an error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            EMF.returnEntityManager(mgr);
            logger.info("entity manager returned");
        }

        return products;
    }

    @POST
    @Path("GetCategories")
    @Produces(MediaType.APPLICATION_JSON)
    public List getProductCategories() {
        EntityManager mgr = null;
        try {
            mgr = EMF.getEntityManager();
            Query query = mgr.createQuery("Select c from Categories c");
            List categories = query.getResultList();
            if (categories != null) {
                logger.info(categories);
                EMF.returnEntityManager(mgr);
                return categories;
            }
            EMF.returnEntityManager(mgr);
        } catch (Exception e) {
            logger.error("getProductCategories threw an error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            EMF.returnEntityManager(mgr);
            logger.info("entity manager returned");
        }

        return null;
    }

    @POST
    @Path("GetProductsByCategory/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List getProductsByCategory(@PathParam("id") Integer id) {
        EntityManager mgr = null;
        try {
            mgr = EMF.getEntityManager();
            if (id != null) {
                Query query = mgr.createQuery("Select pi from ProductInfo pi Where categoryId = " + id);
                List products = query.getResultList();

                if (products != null) {
                    try {

                        List wrappedProducts = new ArrayList();
                        for (int i = 0; i < products.size(); ++i) {
                            wrappedProducts.add(new ProductInfoWrapper((ProductInfo) products.get(i)));
                        }
                        logger.info(products.size());
                        EMF.returnEntityManager(mgr);
                        return wrappedProducts;
                    } catch (Exception e) {
                        logger.error("getProductsByCategory threw an error: " + e.getMessage());
                        e.printStackTrace();
                    } finally {
                        EMF.returnEntityManager(mgr);
                        logger.info("entity manager returned");
                    }
                }
            }
            EMF.returnEntityManager(mgr);
        } catch (Exception e) {
            logger.error("getProductsByCategory threw an error: " + e.getMessage());
            e.printStackTrace();
        }

        return null;
    }

    @POST
    @Path("GetProductInfoById/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ProductInfoWrapper getProductInfoById(@PathParam("id") Integer id) {
        EntityManager mgr = null;
        try {
            mgr = EMF.getEntityManager();
            if (id != null) {
                ProductInfo productInfo = mgr.find(ProductInfo.class, id);
                if (productInfo != null) {
                    EMF.returnEntityManager(mgr);
                    return new ProductInfoWrapper(productInfo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            EMF.returnEntityManager(mgr);
            logger.info("entity manager returned");
        }
        EMF.returnEntityManager(mgr);
        return null;
    }

    @POST
    @Path("UpdateProductInfo/{productId}/{categoryId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Categories updateProductInfo(ProductInfo productInfo, @PathParam("productId") Integer productId, @PathParam("categoryId") Integer categoryId) {

        logger.info(productInfo.toString());

        EntityManager mgr = null;
        try {
            mgr = EMF.getEntityManager();
            EMF.getTransaction(mgr);
            Product prdId = mgr.find(Product.class, productId);
            Categories catId = mgr.find(Categories.class, categoryId);
            productInfo.setProductId(prdId);
            productInfo.setCategoryId(catId);

            if (productInfo != null) {
                EntityTransaction transaction = EMF.getTransaction(mgr);
                mgr.merge(productInfo);
                EMF.commitTransaction(transaction);
                logger.info("returned catId");
                return catId;
            }

        } catch (Exception e) {
            logger.error("updateProductInfo threw an error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            EMF.returnEntityManager(mgr);
            logger.info("entity manager returned");
        }
        logger.info("returned null");
        return null;
    }

    @POST
    @Path("GetProducts")
    @Produces(MediaType.APPLICATION_JSON)
    public List getProducts() {

        EntityManager mgr = null;
        try {
            mgr = EMF.getEntityManager();
            Query query = mgr.createQuery("Select p from Product p");
            List<ProductWrapper> allProductsWrapped = new ArrayList();
            List<Product> products = query.getResultList();
            for (int i = 0; i < products.size(); ++i) {
                Collection<ProductInfoWrapper> productInfoCol = new ArrayList();
                for (ProductInfo pi : products.get(i).getProductInfoCollection()) {
                    productInfoCol.add(new ProductInfoWrapper(pi));
                }
                allProductsWrapped.add(new ProductWrapper(products.get(i), productInfoCol));

                // logger.info(((Product)allProducts.get(i)).getProductInfoCollection());   
                logger.info(products.get(i));
            }

            return allProductsWrapped;

        } catch (Exception e) {
            logger.error("threw an exception" + e.getMessage());
            e.printStackTrace();
        } finally {
            EMF.returnEntityManager(mgr);
            logger.info("entity manager returned");
        }

        return null;

    }

    @POST
    @Path("GetProductInfoByProductId/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ProductInfoWrapper getProductInfoByProductId(@PathParam("id") Integer id) {
        // get product info by product id
        EntityManager mgr = null;
        try {
            mgr = EMF.getEntityManager();
            Query query = mgr.createQuery("Select pi from ProductInfo pi where product_id=" + id);
            ProductInfo singleResult = (ProductInfo) query.getSingleResult();
            logger.info(singleResult.toString());
            if (singleResult != null) {
                ProductInfoWrapper productInfo = new ProductInfoWrapper(singleResult);
                EMF.returnEntityManager(mgr);
                return productInfo;
            }
        } catch (Exception e) {
            logger.error("threw an exception: " + e.getMessage());

        } finally {
            EMF.returnEntityManager(mgr);
            logger.info("entity manager returned");
        }
        return null;
    }

    @POST
    @Path("GetProductById/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ProductWrapper getProductById(@PathParam("id") Integer id) {
        EntityManager mgr = null;
        try {
            mgr = EMF.getEntityManager();
            Product foundProduct = mgr.find(Product.class, id);
            ProductWrapper wrappedProduct = new ProductWrapper(foundProduct);
            EMF.returnEntityManager(mgr);
            return wrappedProduct;
        } catch (Exception e) {
            logger.error("threw an exception " + e.getMessage());
        } finally {
            EMF.returnEntityManager(mgr);
        }
        return null;
    }

    @POST
    @Path("UpdateProduct/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateProduct(@PathParam("id") Integer id, Product product) {
        EntityManager mgr = null;
        try {
            mgr = EMF.getEntityManager();
            Product foundProduct = mgr.find(Product.class, id);
            EntityTransaction tx = EMF.getTransaction(mgr);
            foundProduct.setPrice(product.getPrice());
            foundProduct.setStockQuantity(product.getStockQuantity());
            EMF.commitTransaction(tx);
            EMF.returnEntityManager(mgr);
        } catch (Exception e) {
            logger.error("threw an exception " + e.getMessage());
        } finally {
            EMF.returnEntityManager(mgr);
            logger.info("entity manager returned");
        }
    }

    @POST
    @Path("DeleteProduct/{id}")
    public void deleteProduct(@PathParam("id") Integer id) {
        EntityManager mgr = null;
        try {
            mgr = EMF.getEntityManager();
            Product foundProduct = mgr.find(Product.class, id);
            EntityTransaction tx = EMF.getTransaction(mgr);
            mgr.remove(foundProduct);
            EMF.commitTransaction(tx);
            EMF.returnEntityManager(mgr);
        } catch (Exception e) {
            logger.error("threw an error: " + e.getMessage());
        } finally {
            EMF.returnEntityManager(mgr);
            logger.info("entity manager returned");
        }

    }

    @POST
    @Path("NewProduct")
    @Consumes(MediaType.APPLICATION_JSON)
    public void newProduct(Object newProductData) {
        EntityManager mgr = null;
        logger.info(((HashMap) newProductData).get("name") + " " + ((HashMap) newProductData).get("productDescription"));
        try {
            mgr = EMF.getEntityManager();
            Double price = Double.valueOf(((HashMap) newProductData).get("price").toString());

            Integer categoryId = Integer.valueOf(((HashMap) newProductData).get("categoryId").toString());
            logger.info(categoryId + " cccccccccccccc");
            Product newProduct = new Product();
            newProduct.setPrice(BigDecimal.valueOf(price));
            newProduct.setStockQuantity(0);
            Categories category = mgr.find(Categories.class, categoryId);
            logger.info(category.toString());
            ProductInfo newProductInfo = new ProductInfo();
            newProductInfo.setCategoryId(category);
            newProductInfo.setName((((HashMap) newProductData).get("name")).toString());
            newProductInfo.setManufacturer((((HashMap) newProductData).get("manufacturer")).toString());
            newProductInfo.setProductDescription((((HashMap) newProductData).get("productDescription")).toString());
            newProductInfo.setProductId(newProduct);

            EntityTransaction tx = EMF.getTransaction(mgr);
            mgr.persist(newProduct);
            mgr.persist(newProductInfo);
            EMF.commitTransaction(tx);

        } catch (Exception e) {
            logger.error("threw an error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            EMF.returnEntityManager(mgr);
            logger.info("entity manager returned");
        }

    }

    @POST
    @Path("AddRecieval")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addRecieval(HashMap recievalData) {
        logger.info(recievalData.get("restockDate"));
        EntityManager mgr = null;
        try {
            mgr = EMF.getEntityManager();
            Restock restock = new Restock();
            restock.setRestockDate(Date.valueOf(recievalData.get("restockDate").toString()));
            restock.setRestockInfo(recievalData.get("restockInfo").toString());

            RestockInfo restockInfo = new RestockInfo();
            restockInfo.setRestockId(restock);
            restockInfo.setQuantity(Integer.valueOf(recievalData.get("quantity").toString()));
            Product product = mgr.find(Product.class, Integer.valueOf(recievalData.get("productId").toString()));
            restockInfo.setProductId(product);
            Integer currentQuantity = product.getStockQuantity();
            product.setStockQuantity(currentQuantity + restockInfo.getQuantity());

            EntityTransaction tx = EMF.getTransaction(mgr);
            mgr.persist(restock);
            mgr.persist(restockInfo);
            mgr.persist(product);
            logger.info("commited");
            EMF.commitTransaction(tx);

        } catch (Exception e) {
            logger.info("threw an exception " + e.getMessage());
        } finally {
            EMF.returnEntityManager(mgr);
            logger.info("entity manager returned");
        }
    }

}
