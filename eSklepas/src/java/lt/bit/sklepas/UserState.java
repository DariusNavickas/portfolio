/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit.sklepas;

/**
 *
 * @author Gebruiker
 */
public class UserState {

    private short userType;
    private short loggedIn;
    

    public UserState(short userType, short loggedIn) {
        this.userType = userType;
        this.loggedIn = loggedIn;
    }
    public UserState(){}

    public short getUserType() {
        return userType;
    }

    public short isLoggedIn() {
        return loggedIn;
    }
    
}
