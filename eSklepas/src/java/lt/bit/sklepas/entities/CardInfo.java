/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit.sklepas.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Gebruiker
 */
@Entity
@Table(name = "card_info")
@NamedQueries({
    @NamedQuery(name = "CardInfo.findAll", query = "SELECT c FROM CardInfo c")
    , @NamedQuery(name = "CardInfo.findByCardInfoId", query = "SELECT c FROM CardInfo c WHERE c.cardInfoId = :cardInfoId")
    , @NamedQuery(name = "CardInfo.findByCardNumber", query = "SELECT c FROM CardInfo c WHERE c.cardNumber = :cardNumber")
    , @NamedQuery(name = "CardInfo.findByExpDate", query = "SELECT c FROM CardInfo c WHERE c.expDate = :expDate")
    , @NamedQuery(name = "CardInfo.findByCvv", query = "SELECT c FROM CardInfo c WHERE c.cvv = :cvv")
    , @NamedQuery(name = "CardInfo.findByOwnerFirstName", query = "SELECT c FROM CardInfo c WHERE c.ownerFirstName = :ownerFirstName")
    , @NamedQuery(name = "CardInfo.findByOwnerLastName", query = "SELECT c FROM CardInfo c WHERE c.ownerLastName = :ownerLastName")})
public class CardInfo implements Serializable {

    private static final long serialVersionUID = 10L;

    private Integer cardInfoId;

    private String cardNumber;

    private String expDate;

    private String cvv;

    private String ownerFirstName;

    private String ownerLastName;

    private Users userId;

    public CardInfo() {
    }

    public CardInfo(Integer cardInfoId) {
        this.cardInfoId = cardInfoId;
    }

    public CardInfo(Integer cardInfoId, String cardNumber, String expDate, String cvv, String ownerFirstName, String ownerLastName) {
        this.cardInfoId = cardInfoId;
        this.cardNumber = cardNumber;
        this.expDate = expDate;
        this.cvv = cvv;
        this.ownerFirstName = ownerFirstName;
        this.ownerLastName = ownerLastName;
    }
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "card_info_id")
    public Integer getCardInfoId() {
        return cardInfoId;
    }

    public void setCardInfoId(Integer cardInfoId) {
        this.cardInfoId = cardInfoId;
    }
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "card_number")
    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "exp_date")
    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "cvv")
    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "owner_first_name")
    public String getOwnerFirstName() {
        return ownerFirstName;
    }

    public void setOwnerFirstName(String ownerFirstName) {
        this.ownerFirstName = ownerFirstName;
    }
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "owner_last_name")
    public String getOwnerLastName() {
        return ownerLastName;
    }

    public void setOwnerLastName(String ownerLastName) {
        this.ownerLastName = ownerLastName;
    }
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false)
    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cardInfoId != null ? cardInfoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CardInfo)) {
            return false;
        }
        CardInfo other = (CardInfo) object;
        if ((this.cardInfoId == null && other.cardInfoId != null) || (this.cardInfoId != null && !this.cardInfoId.equals(other.cardInfoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lt.bit.sklepas.entities.CardInfo[ cardInfoId=" + cardInfoId + " ]";
    }
    
}
