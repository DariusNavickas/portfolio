/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit.sklepas.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Gebruiker
 */
@Entity
@Table(name = "restock")
@NamedQueries({
    @NamedQuery(name = "Restock.findAll", query = "SELECT r FROM Restock r")
    , @NamedQuery(name = "Restock.findByRestockId", query = "SELECT r FROM Restock r WHERE r.restockId = :restockId")
    , @NamedQuery(name = "Restock.findByRestockDate", query = "SELECT r FROM Restock r WHERE r.restockDate = :restockDate")
    , @NamedQuery(name = "Restock.findByRestockInfo", query = "SELECT r FROM Restock r WHERE r.restockInfo = :restockInfo")})
public class Restock implements Serializable {

    private static final long serialVersionUID = 5L;

    private Integer restockId;

    private Date restockDate;

    private String restockInfo;

    private Collection<RestockInfo> restockInfoCollection;

    public Restock() {
    }

    public Restock(Integer restockId) {
        this.restockId = restockId;
    }

    public Restock(Integer restockId, Date restockDate) {
        this.restockId = restockId;
        this.restockDate = restockDate;
    }

    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "restock_id")
    public Integer getRestockId() {
        return restockId;
    }

    public void setRestockId(Integer restockId) {
        this.restockId = restockId;
    }

    @Basic(optional = false)
    @NotNull
    @Column(name = "restock_date")
    @Temporal(TemporalType.DATE)
    public Date getRestockDate() {
        return restockDate;
    }

    public void setRestockDate(Date restockDate) {
        this.restockDate = restockDate;
    }

    @Size(max = 255)
    @Column(name = "restock_info")
    public String getRestockInfo() {
        return restockInfo;
    }

    public void setRestockInfo(String restockInfo) {
        this.restockInfo = restockInfo;
    }

    @OneToMany(mappedBy = "restockId")
    public Collection<RestockInfo> getRestockInfoCollection() {
        return restockInfoCollection;
    }

    public void setRestockInfoCollection(Collection<RestockInfo> restockInfoCollection) {
        this.restockInfoCollection = restockInfoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (restockId != null ? restockId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Restock)) {
            return false;
        }
        Restock other = (Restock) object;
        if ((this.restockId == null && other.restockId != null) || (this.restockId != null && !this.restockId.equals(other.restockId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lt.bit.sklepas.entities.Restock[ restockId=" + restockId + " ]";
    }

}
