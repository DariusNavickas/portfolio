/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit.sklepas.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Gebruiker
 */
@Entity
@Table(name = "restock_info")
@NamedQueries({
    @NamedQuery(name = "RestockInfo.findAll", query = "SELECT r FROM RestockInfo r")
    , @NamedQuery(name = "RestockInfo.findByRestockInfoId", query = "SELECT r FROM RestockInfo r WHERE r.restockInfoId = :restockInfoId")
    , @NamedQuery(name = "RestockInfo.findByQuantity", query = "SELECT r FROM RestockInfo r WHERE r.quantity = :quantity")})
public class RestockInfo implements Serializable {

    private static final long serialVersionUID = 4L;

    private Integer restockInfoId;

    private Integer quantity;

    private Restock restockId;

    private Product productId;

    public RestockInfo() {
    }

    public RestockInfo(Integer restockInfoId) {
        this.restockInfoId = restockInfoId;
    }

    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "restock_info_id")
    public Integer getRestockInfoId() {
        return restockInfoId;
    }

    public void setRestockInfoId(Integer restockInfoId) {
        this.restockInfoId = restockInfoId;
    }

    @Column(name = "quantity")
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @JoinColumn(name = "restock_id", referencedColumnName = "restock_id")
    @ManyToOne
    public Restock getRestockId() {
        return restockId;
    }

    public void setRestockId(Restock restockId) {
        this.restockId = restockId;
    }

    @JoinColumn(name = "product_id", referencedColumnName = "product_id")
    @ManyToOne
    public Product getProductId() {
        return productId;
    }

    public void setProductId(Product productId) {
        this.productId = productId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (restockInfoId != null ? restockInfoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RestockInfo)) {
            return false;
        }
        RestockInfo other = (RestockInfo) object;
        if ((this.restockInfoId == null && other.restockInfoId != null) || (this.restockInfoId != null && !this.restockInfoId.equals(other.restockInfoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lt.bit.sklepas.entities.RestockInfo[ restockInfoId=" + restockInfoId + " ]";
    }

}
