/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit.sklepas.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Gebruiker
 */
@Entity
@Table(name = "cart_info")
@NamedQueries({
    @NamedQuery(name = "CartInfo.findAll", query = "SELECT c FROM CartInfo c")
    , @NamedQuery(name = "CartInfo.findByCartInfoId", query = "SELECT c FROM CartInfo c WHERE c.cartInfoId = :cartInfoId")
    , @NamedQuery(name = "CartInfo.findByQuantity", query = "SELECT c FROM CartInfo c WHERE c.quantity = :quantity")
    , @NamedQuery(name = "CartInfo.findByPrice", query = "SELECT c FROM CartInfo c WHERE c.price = :price")})
public class CartInfo implements Serializable {

    private static final long serialVersionUID = 9L;

    private Integer cartInfoId;

    private Integer quantity;

    private BigDecimal price;

    private ShoppingCart cartId;

    private Product productId;

    public CartInfo() {
    }

    public CartInfo(Integer cartInfoId) {
        this.cartInfoId = cartInfoId;
    }
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    @Column(name = "cart_info_id")
    public Integer getCartInfoId() {
        return cartInfoId;
    }

    public void setCartInfoId(Integer cartInfoId) {
        this.cartInfoId = cartInfoId;
    }
    @Column(name = "quantity")
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    @Column(name = "price")
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    @JoinColumn(name = "cart_id", referencedColumnName = "cart_id")
    @ManyToOne(optional = false)
    public ShoppingCart getCartId() {
        return cartId;
    }

    public void setCartId(ShoppingCart cartId) {
        this.cartId = cartId;
    }
    @JoinColumn(name = "product_id", referencedColumnName = "product_id")
    @ManyToOne(optional = false)
    public Product getProductId() {
        return productId;
    }

    public void setProductId(Product productId) {
        this.productId = productId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cartInfoId != null ? cartInfoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CartInfo)) {
            return false;
        }
        CartInfo other = (CartInfo) object;
        if ((this.cartInfoId == null && other.cartInfoId != null) || (this.cartInfoId != null && !this.cartInfoId.equals(other.cartInfoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lt.bit.sklepas.entities.CartInfo[ cartInfoId=" + cartInfoId + " ]";
    }
    
}
