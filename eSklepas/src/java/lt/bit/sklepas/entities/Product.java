/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit.sklepas.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Gebruiker
 */
@Entity
@Table(name = "product")
@NamedQueries({
    @NamedQuery(name = "Product.findAll", query = "SELECT p FROM Product p")
    , @NamedQuery(name = "Product.findByProductId", query = "SELECT p FROM Product p WHERE p.productId = :productId")
    , @NamedQuery(name = "Product.findByStockQuantity", query = "SELECT p FROM Product p WHERE p.stockQuantity = :stockQuantity")
    , @NamedQuery(name = "Product.findByPrice", query = "SELECT p FROM Product p WHERE p.price = :price")})
public class Product implements Serializable {

    private static final long serialVersionUID = 7L;

    private Integer productId;

    private Integer stockQuantity;

    private BigDecimal price;

    private Collection<CartInfo> cartInfoCollection;

    private Collection<ProductInfo> productInfoCollection;

    private Collection<RestockInfo> restockInfoCollection;

    public Product() {
    }

    public Product(Integer productId) {
        this.productId = productId;
    }
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    @Column(name = "product_id")
    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }
    @Column(name = "stock_quantity")
    public Integer getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(Integer stockQuantity) {
        this.stockQuantity = stockQuantity;
    }
    @Column(name = "price")
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productId", fetch=FetchType.EAGER)
    @Transient
    public Collection<CartInfo> getCartInfoCollection() {
        return cartInfoCollection;
    }

    public void setCartInfoCollection(Collection<CartInfo> cartInfoCollection) {
        this.cartInfoCollection = cartInfoCollection;
    }
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productId" , fetch=FetchType.LAZY)
   
    public Collection<ProductInfo> getProductInfoCollection() {
        return productInfoCollection;
    }

    public void setProductInfoCollection(Collection<ProductInfo> productInfoCollection) {
        this.productInfoCollection = productInfoCollection;
    }
    @OneToMany(mappedBy = "productId" , fetch=FetchType.EAGER)
    @Transient
    public Collection<RestockInfo> getRestockInfoCollection() {
        return restockInfoCollection;
    }

    public void setRestockInfoCollection(Collection<RestockInfo> restockInfoCollection) {
        this.restockInfoCollection = restockInfoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productId != null ? productId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Product)) {
            return false;
        }
        Product other = (Product) object;
        if ((this.productId == null && other.productId != null) || (this.productId != null && !this.productId.equals(other.productId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lt.bit.sklepas.entities.Product[ productId=" + productId + " ]";
    }
    
}
