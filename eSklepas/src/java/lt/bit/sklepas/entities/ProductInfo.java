/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit.sklepas.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Gebruiker
 */
@Entity
@Table(name = "product_info")
@NamedQueries({
    @NamedQuery(name = "ProductInfo.findAll", query = "SELECT p FROM ProductInfo p")
    , @NamedQuery(name = "ProductInfo.findByProductInfoId", query = "SELECT p FROM ProductInfo p WHERE p.productInfoId = :productInfoId")
    , @NamedQuery(name = "ProductInfo.findByName", query = "SELECT p FROM ProductInfo p WHERE p.name = :name")
    , @NamedQuery(name = "ProductInfo.findByManufacturer", query = "SELECT p FROM ProductInfo p WHERE p.manufacturer = :manufacturer")
    , @NamedQuery(name = "ProductInfo.findByProductDescription", query = "SELECT p FROM ProductInfo p WHERE p.productDescription = :productDescription")})
public class ProductInfo implements Serializable {

    private static final long serialVersionUID = 6L;

    private Integer productInfoId;

    private String name;

    private String manufacturer;

    private String productDescription;

    private Categories categoryId;

    private Product productId;

    public ProductInfo() {
    }

    public ProductInfo(Integer productInfoId) {
        this.productInfoId = productInfoId;
    }
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_info_id")
    public Integer getProductInfoId() {
        return productInfoId;
    }

    public void setProductInfoId(Integer productInfoId) {
        this.productInfoId = productInfoId;
    }
    @Size(max = 255)
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Size(max = 255)
    @Column(name = "manufacturer")
    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
    @Size(max = 255)
    @Column(name = "product_description")
    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }
    @JoinColumn(name = "category_id", referencedColumnName = "category_id")
    @ManyToOne
    
    public Categories getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Categories categoryId) {
        this.categoryId = categoryId;
    }
    
    @JoinColumn(name = "product_id", referencedColumnName = "product_id")
    @ManyToOne(optional = false)
    public Product getProductId() {
        return productId;
    }

    public void setProductId(Product productId) {
        this.productId = productId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productInfoId != null ? productInfoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductInfo)) {
            return false;
        }
        ProductInfo other = (ProductInfo) object;
        if ((this.productInfoId == null && other.productInfoId != null) || (this.productInfoId != null && !this.productInfoId.equals(other.productInfoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lt.bit.sklepas.entities.ProductInfo[ productInfoId=" + productInfoId + " ]";
    }
    
}
