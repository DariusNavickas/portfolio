/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit.sklepas.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Gebruiker
 */
@Entity
@Table(name = "shopping_cart")
@NamedQueries({
    @NamedQuery(name = "ShoppingCart.findAll", query = "SELECT s FROM ShoppingCart s")
    , @NamedQuery(name = "ShoppingCart.findByCartId", query = "SELECT s FROM ShoppingCart s WHERE s.cartId = :cartId")
    , @NamedQuery(name = "ShoppingCart.findByDateOfPurchase", query = "SELECT s FROM ShoppingCart s WHERE s.dateOfPurchase = :dateOfPurchase")
    , @NamedQuery(name = "ShoppingCart.findByDateOfCreation", query = "SELECT s FROM ShoppingCart s WHERE s.dateOfCreation = :dateOfCreation")})
public class ShoppingCart implements Serializable {

    private static final long serialVersionUID = 3L;

    private Integer cartId;

    private Date dateOfPurchase;

    private Date dateOfCreation;

    private Collection<CartInfo> cartInfoCollection;

    private Users userId;

    public ShoppingCart() {
    }

    public ShoppingCart(Integer cartId) {
        this.cartId = cartId;
    }

    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    @Column(name = "cart_id")
    public Integer getCartId() {
        return cartId;
    }

    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    @Column(name = "date_of_purchase")
    @Temporal(TemporalType.DATE)
    public Date getDateOfPurchase() {
        return dateOfPurchase;
    }

    public void setDateOfPurchase(Date dateOfPurchase) {
        this.dateOfPurchase = dateOfPurchase;
    }

    @Column(name = "date_of_creation")
    @Temporal(TemporalType.DATE)
    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cartId")
    public Collection<CartInfo> getCartInfoCollection() {
        return cartInfoCollection;
    }

    public void setCartInfoCollection(Collection<CartInfo> cartInfoCollection) {
        this.cartInfoCollection = cartInfoCollection;
    }

    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = true)
    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cartId != null ? cartId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ShoppingCart)) {
            return false;
        }
        ShoppingCart other = (ShoppingCart) object;
        if ((this.cartId == null && other.cartId != null) || (this.cartId != null && !this.cartId.equals(other.cartId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ShoppingCart{" + "cartId=" + cartId + ", dateOfPurchase=" + dateOfPurchase + ", dateOfCreation=" + dateOfCreation +  ", userId=" + userId.getUserId() + '}';
    }






}
