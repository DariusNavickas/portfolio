/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit.sklepas;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import lt.bit.sklepas.entities.*;

/**
 *
 * @author Gebruiker
 */
@Path("GetData")
public class GetData {
    
    
    
    @Path("GetUserData")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Users> getUserData(){
        EntityManager mgr = EMF.getEntityManager();
        Query q = mgr.createQuery("Select u from Users u");
        List users = q.getResultList();
    return users;
    }
    
    
    
    
}
