/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entityWrappers;

import java.math.BigDecimal;
import lt.bit.sklepas.entities.CartInfo;
import lt.bit.sklepas.entities.Product;
import lt.bit.sklepas.entities.ShoppingCart;

/**
 *
 * @author Gebruiker
 */
public class CartInfoWrapper {
    
     private Integer cartInfoId;

    private Integer quantity;

    private BigDecimal price;

    private Integer cartId;

    private Integer productId;
    
    private final String productName;

    public String getProductName() {
        return productName;
    }
    

    public CartInfoWrapper(CartInfo cartInfo, String productName) {
        this.cartInfoId = cartInfo.getCartInfoId();
        this.quantity = cartInfo.getQuantity();
        this.price = cartInfo.getPrice();
        this.cartId = cartInfo.getCartId().getCartId();
        this.productId = cartInfo.getProductId().getProductId();
        this.productName = productName;
    }
    
    

    public Integer getCartInfoId() {
        return cartInfoId;
    }

    public void setCartInfoId(Integer cartInfoId) {
        this.cartInfoId = cartInfoId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getCartId() {
        return cartId;
    }

    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }
    
    

    
}
