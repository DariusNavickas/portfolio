/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entityWrappers;


import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import lt.bit.sklepas.entities.Product;
import lt.bit.sklepas.entities.ProductInfo;

/**
 *
 * @author Gebruiker
 */
public class ProductWrapper {

    private Integer productId;

    private Integer stockQuantity;

    private BigDecimal price;

    private Collection<ProductInfoWrapper> productInfo;

    public ProductWrapper(Product product, Collection<ProductInfoWrapper> info) {
        this.productId = product.getProductId();
        this.stockQuantity = product.getStockQuantity();
        this.productInfo = info;
        this.price = product.getPrice();
    }

    public ProductWrapper(Product product) {
        this.productId = product.getProductId();
        this.stockQuantity = product.getStockQuantity();
        this.price = product.getPrice();
    }

    public ProductWrapper() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(Integer stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Collection getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(List productInfo) {
        this.productInfo = productInfo;
    }

    @Override
    public String toString() {
        return "ProductWrapper{" + "productId=" + productId + ", stockQuantity=" + stockQuantity + ", price=" + price + ", productInfo=" + productInfo + '}';
    }

}
