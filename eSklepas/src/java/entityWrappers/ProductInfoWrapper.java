/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entityWrappers;

import lt.bit.sklepas.entities.Categories;
import lt.bit.sklepas.entities.Product;
import lt.bit.sklepas.entities.ProductInfo;

/**
 *
 * @author Gebruiker
 */
public class ProductInfoWrapper {
    
    private Integer productInfoId;

    private String name;

    private String manufacturer;

    private String productDescription;

    private Integer categoryId;

    private Integer productId;

    
    public ProductInfoWrapper(ProductInfo f){
        this.productInfoId = f.getProductInfoId();
        this.name = f.getName();
        this.manufacturer = f.getManufacturer();
        this.productDescription = f.getProductDescription();
        this.categoryId = f.getCategoryId().getCategoryId();
        this.productId = f.getProductId().getProductId();
    
    }


    public Integer getProductInfoId() {
        return productInfoId;
    }

    public void setProductInfoId(Integer productInfoId) {
        this.productInfoId = productInfoId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }
    
    
}
