/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function getProductCategories() {
    $("#data").empty();
    request = $.ajax({
        method: "POST",

        beforeSend: function (request) {
            request.setRequestHeader("Accept", "application/json");
        },
        url: "/eSklepas/rest/AdminPanel/GetCategories",

        success: function (response) {
            $("#categories").empty();
            console.log("Entered", response);

            let buttons = "";
            for (let i = 0; i < response.length; ++i) {
                buttons += "<button type='button' onclick='getProductsByCategory(" + response[i].categoryId + ",\"" + response[i].categoryName + "\")'; value=" + response[i].categoryId + ">" + response[i].categoryName + "</button>";
            }
            console.log(buttons);

            $("#categories").append(buttons);
        },
        error: function (error) {
            console.log("error", error);
        }
    });
}

function getProductsByCategory(id, catName) {
    request = $.ajax({
        method: "POST",
        beforeSend: function (request) {
            request.setRequestHeader("Accept", "application/json");
        },
        url: "/eSklepas/rest/AdminPanel/GetProductsByCategory/" + id,

        success: function (response) {
            $("#data").empty();
            console.log("success", response);
            let table = "<table class='table table-bordered table-hover table-striped'>";
            table += "<thead>";
            for (let i = 0; i < Object.keys(response[0]).length; ++i) {
                table += "<th>" + Object.keys(response[0])[i] + "</th>";
            }
            table += "</thead>";
            for (let j = 0; j < response.length; ++j) {
                table += "<tr>";
                for (let k = 0; k < Object.values(response[j]).length; ++k) {
                    table += "<td>" + Object.values(response[j])[k] + "</td>";
                }
                table += "<td><button onclick='editProductInfo(" + response[j].productId + ")'; data-toggle='modal' data-target='#modal'>edit" + response[j].productId + "</button></td>";
                table += "</tr>";
            }
            table += "</table>";
            console.log(Object.keys(response[0]));
            $("#data").append(table);
        },
        error: function (error) {
            console.log("error", error);
        }
    });
}

function editProductInfo(id) {
    request = $.ajax({

        method: "POST",

        beforeSend: function (request) {
            request.setRequestHeader("Accept", "application/json");
        },
        url: "/eSklepas/rest/AdminPanel/GetProductInfoById/" + id,

        success: function (response) {
            console.log(response);
            $(".modal-body").empty();
            let form = "<form class='form-group'>";
            form += "<label>category id</label>";
            form += "<input class='form-control' type='text' id='CATEGORY_ID'>";
            form += "<input class='form-control' type='text' id='NAME'>";
            form += "<input class='form-control' type='text' id='PRODUCT_DESCRIPTION'>";
            form += "<input class='form-control' type='text' id='MANUFACTURER'>";
            form += "<button type='button' onclick='updateProductInfo(" + response.productInfoId + "," + response.productId + ");' data-dismis='modal'>Save</button>";
            form += "</form>";

            $(".modal-body").append(form);

            $("#CATEGORY_ID").val(response.categoryId);
            $("#NAME").val(response.name);
            $("#PRODUCT_DESCRIPTION").val(response.productDescription);
            $("#MANUFACTURER").val(response.manufacturer);

        },
        error: function (error) {
            console.log("error", error);
        }
    });

    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus');
    });
}

function updateProductInfo(productInfoId, productId) {
    let productInfo = {
    };
    productInfo.productInfoId = productInfoId;
    productInfo.name = $("#NAME").val();
    productInfo.manufacturer = $("#MANUFACTURER").val();
    productInfo.productDescription = $("#PRODUCT_DESCRIPTION").val();
    console.log(productInfo);
    request = $.ajax({
        method: "POST",
        beforeSend: function (request) {
            request.setRequestHeader("Content-Type", "application/json");
            request.setRequestHeader("Accept", "application/json");
        },
        url: "/eSklepas/rest/AdminPanel/UpdateProductInfo/" + productId + "/" + $("#CATEGORY_ID").val(),
        dataType: "json",
        data: JSON.stringify(productInfo),

        success: function (response) {
            console.log("successfully updated product info", response);
            if (response.categoryId != null && response.categoryName != null) {
                getProductsByCategory(response.categoryId, response.categoryName);
                $("#modal").modal("toggle");
            }

        },
        error: function (error) {
            console.log("error", error);
        }
    });
}


function getProducts() {
    $("#data").empty();
    $("#categories").empty();

    request = $.ajax({
        method: "POST",

        beforeSend: function (request) {
            request.setRequestHeader("Accept", "application/json");
        },
        url: "/eSklepas/rest/AdminPanel/GetProducts",

        success: function (allProducts) {

            console.log("Entered", allProducts);

            if (allProducts != null) {
                let  table = "<button type='button' onclick='newProduct();' data-target='#modal' data-toggle='modal'>New Product</button>";
                table += "<table class='table table-bordered table-hover table-striped'>";
                table += "<thead>";
                for (let i = 0; i < Object.keys(allProducts[0]).length; ++i) {
                    table += "<th>" + Object.keys(allProducts[0])[i] + "</th>";
                }
                table += "</thead>";
                for (let j = 0; j < allProducts.length; ++j) {
                    table += "<tr>";
                    for (let k = 0; k < Object.values(allProducts[j]).length; ++k) {
                        if (Object.keys(allProducts[j])[k] != "productInfo") {
                            table += "<td>" + Object.values(allProducts[j])[k] + "</td>";
                        } else {
                            let list = Object.values(allProducts[j])[2];
                            console.log(list);
                            table += "<td><button type='button' class='productInfoBtn' id='product" + Object.values(allProducts[j])[1] + "'>Product Info</button></td>";
                            let id = Object.values(allProducts[j])[1];

                        }
                    }
                    table += "<td><button onclick='editProduct(" + Object.values(allProducts[j])[1] + ")'; data-toggle='modal' data-target='#modal'>Edit</button></td>";
                    table += "<td><button onclick='deleteProduct(" + Object.values(allProducts[j])[1] + ")';>Delete</button></td>";
                    table += "</tr>";
                }


                table += "</table>";
                table += "<div id='dialog' title='Product Info'></div>";

                $("#data").append(table);
                $(".productInfoBtn").each(function () {
                    $(this).click(function () {
                        let productId = $(this).attr('id').replace("product", "");
                        showProductInfo(productId);
                        console.log(productId);
                    });
                });
                $("#dialog").dialog({
                    minWidth: 870,
                    autoOpen: false,
                    open: 'enabled'
                });

            }
        },
        error: function (error) {
            console.log("error", error);
        }
    });
}

function deleteProduct(id) {
    request = $.ajax({
        method: "POST",
        url: "/eSklepas/rest/AdminPanel/DeleteProduct/" + id,
        success: function (response) {
            console.log("deleted", response);


        },
        error: function (error) {
            console.log("error", error);
        }
    });
}


function editProduct(id) {
    request = $.ajax({
        method: "POST",
        beforeSend: function (request) {
            request.setRequestHeader("Accept", "application/json");
        },
        url: "/eSklepas/rest/AdminPanel/GetProductById/" + id,
        success: function (product) {
            console.log("productReceived", product);
            $(".modal-body").empty();
            let form = "<form class='form-group'>";
            form += "<input class='form-control' type='text' id='PRICE'>";
            form += "<input class='form-control' type='text' id='STOCK_QUANTITY'>";
            form += "<button type='button' onclick='updateProduct(" + product.productId + ");' data-dismis='modal'>Save</button>";
            form += "</form>";
            $(".modal-body").append(form);
            $("#PRICE").val(product.price);
            $("#STOCK_QUANTITY").val(product.stockQuantity);
        },
        error: function (error) {
            console.log("error ", error);
        }
    });
}

function updateProduct(id) {
    product = {};
    product.price = $("#PRICE").val();
    product.stockQuantity = $("#STOCK_QUANTITY").val();

    request = $.ajax({
        method: "POST",
        beforeSend: function (request) {
            request.setRequestHeader("Content-Type", "application/json");
        },

        url: "/eSklepas/rest/AdminPanel/UpdateProduct/" + id,
        dataType: "json",
        data: JSON.stringify(product),
        success: function (response) {
            console.log("updated successfully", response);
            $("#modal").modal("toggle");
        },
        error: function (error) {
            console.log("error updating product", error);
        }
    });
}

function newProduct() {
    let form = "<form class='form-group'>";
    form += "<input class='form-control' type='text' placeholder='category_id' id='CATEGORY_ID'>"; //pakeist i select
    form += "<input class='form-control' type='text' placeholder='Price' id='PRICE'>";
    form += "<input class='form-control' type='text' placeholder='Product name' id='NAME'>";
    form += "<input class='form-control' type='text' placeholder='Manufacturer' id='MANUFACTURER'>";
    form += "<input class='form-control' type='text' placeholder='Product description' id='PRODUCT_DESCRIPTION'>";
    form += "<button type='button' onclick='addNewProduct();'>Add</button>";
    form += "</form>";

    $(".modal-body").empty();
    $(".modal-body").append(form);


}

function addNewProduct() {
    let newProduct = {};


    newProduct.categoryId = $("#CATEGORY_ID").val();
    newProduct.name = $("#NAME").val();
    newProduct.manufacturer = $("#MANUFACTURER").val();
    newProduct.productDescription = $("#PRODUCT_DESCRIPTION").val();
    newProduct.price = $("#PRICE").val();
    newProduct.stockQuantity = 0;

    request = $.ajax({
        method: "POST",
        beforeSend: function (request) {
            request.setRequestHeader("Content-Type", "application/json");
        },
        url: "/eSklepas/rest/AdminPanel/NewProduct",
        dataType: "json",
        data: JSON.stringify(newProduct),
        success: function (response) {
            console.log("success", response);
        },
        error: function (error) {
            console.log("error", error);
        }
    });


}


function showProductInfo(productId) {

    request = $.ajax({
        method: "POST",
        beforeSend: function (request) {
            request.setRequestHeader("Accept", "application/json");
        },
        url: "/eSklepas/rest/AdminPanel/GetProductInfoByProductId/" + productId,

        success: function (productInfo) {
            console.log("success", productInfo);
            let table = "<table class='table table-bordered table-hover table-striped'>";
            table += "<thead>";
            for (let i = 0; i < Object.keys(productInfo).length; ++i) {
                table += "<th>" + Object.keys(productInfo)[i] + "</th>";
            }
            table += "</thead>";

            table += "<tr>";
            for (let k = 0; k < Object.values(productInfo).length; ++k) {

                table += "<td>" + Object.values(productInfo)[k] + "</td>";

            }
            table += "<td><button onclick='editProductInfo(" + productInfo.productId + ")'; data-toggle='modal' data-target='#modal'>Edit</button></td>";
            table += "</tr>";

            table += "</table>";
            $("#dialog").empty();
            $("#dialog").append(table);
            $("#dialog").dialog("open");
        },
        error: function (error) {
            console.log("error", error);
        }
    });
}


function restockProduct() {

    request = $.ajax({
        method: "POST",

        beforeSend: function (request) {
            request.setRequestHeader("Accept", "application/json");
        },
        url: "/eSklepas/rest/AdminPanel/GetProducts",

        success: function (allProducts) {
            console.log("aallll products", allProducts);
            let form = "<form class='form-group'>";
            form +="<label>Product</label>";
            form += "<select name='Product' class='form-control' id='PRODUCT_ID'>";
            for (i = 0; i < Object.keys(allProducts).length; ++i) {
                console.log(Object.values(allProducts)[i].productId);
                console.log(Object.values(allProducts)[i].productInfo[0].name);
                form += "<option value=" + Object.values(allProducts)[i].productId + ">" + Object.values(allProducts)[i].productInfo[0].name + "</option>";
            }
            form += "</select>";
            form +="<label>Quantity</label>";
            form += "<input type='number' class='form-control' id='STOCK_QUANTITY' placeholder='quantity'>";
            form +="<label>Additional Info</label>";
            form += "<input type='text-area' class='form-control' placeholde='Additional info' id='RESTOCK_INFO'>";
            form +="<label>Date of recieval</label>";
            form += "<input type='date' class='form-control' placeholder='date of recieval' id='RESTOCK_DATE'>";
            form += "<button type='button' onclick='addRecieval();'>Add</button>";
            form += "</form>";
            
            $(".modal-body").empty();
            $(".modal-body").append(form);

        },

        error: function (error) {}
    });

}

function addRecieval(){
    let recievalData = {};
    recievalData.productId = $("#PRODUCT_ID").val();
    recievalData.quantity = $("#STOCK_QUANTITY").val();
    recievalData.restockInfo = $("#RESTOCK_INFO").val();
    recievalData.restockDate = $("#RESTOCK_DATE").val();
    
      request = $.ajax({
        method: "POST",

        beforeSend: function (request) {
            request.setRequestHeader("Content-Type", "application/json");
        },
        url: "/eSklepas/rest/AdminPanel/AddRecieval",
        dataType: 'json',
        data: JSON.stringify(recievalData),
        success: function (allProducts) {
            console.log("success on sending recieval to the server");
        },
        
        error: function (error){
            console.log("error adding recieval", error);
        }
    });
    
    
}