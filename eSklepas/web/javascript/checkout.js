/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(()=>{
    console.log("cia uzkraunam shopping carta");
    getShoppingCartData();
});





function getShoppingCartData(){
      request = $.ajax({
        beforeSend: function(request){request.setRequestHeader("Accept","application/json;charset=UTF-8");},
        method: "POST",
        url: "rest/ShoppingController/GetShoppingCartData/",
        success: function (shoppingCartInfo) {
            console.log("gavom shoppingCartData", shoppingCartInfo);
            let table = "<table class='table table-bordered table-hover table-striped'>";
            table+= "<thead>";
            table+="<th>Product Name</th>";
            table+="<th>Quantity</th>";
            table+="<th>Price</th></thead>";
            total =0;
            if(shoppingCartInfo != null){
                   for(i=0; i<shoppingCartInfo.length;++i){
                     total += shoppingCartInfo[i].price;
                table+="<tr>";
                table+="<td>"+shoppingCartInfo[i].productName+"</td>";
                table+="<td><input type='number' value='"+shoppingCartInfo[i].quantity+"' class='quantityValue' id="+shoppingCartInfo[i].cartInfoId+"></input></td>";
                table+="<td>"+shoppingCartInfo[i].price+"</td>";
                table+="<td><button onclick='removeShoppingCartInfo("+shoppingCartInfo[i].cartInfoId+");'>Remove</button></td>";
                table+="</tr>";
                
            }
          
            $("#cartData").empty();
            $("#cartData").append(table);
            $(".quantityValue").change(function(){changeQuantity($(this).attr("id"),$(this).val())});
            $("#cartData").append("<div class='float-right'>Grand Total: "+total.toFixed(2)+"</div>");
            $("#cartData").append("<button type='button' onclick='checkOut();'>Check Out</button>");
            }else{
                $("#cartData").empty();
                $("#cartData").append("Your shopping cart is empty");
            }
         
        },
        error: function (error) {
            console.log("gavom errora is shopping cart data");
        }
        
    });
}
function changeQuantity(id, quantity){
    console.log("changing quantity for "+id + "to quantity= "+quantity);
    request = $.ajax({
        method:"POST",
        url: "rest/ShoppingController/ChangeQuantity/"+id+"?quantity="+quantity,
        success: function(response){
            console.log("successfully updated quantity");
            window.location.reload();
        },
        error: function(error){
            console.log("error updating quantity");
        }
    });
    
    
}

function removeShoppingCartInfo(id){
    console.log("remove "+ id);
    
        request = $.ajax({
        method: "POST",
        url: "rest/ShoppingController/RemoveShoppingCartInfo/"+id,
        success: function (response) {
            console.log("removed shopping cart info");
            window.location.reload();
            
        },
        error: function (error) {
            console.log("gavom errora is shopping cart");
        }
    });
    
}