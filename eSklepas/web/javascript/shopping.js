/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(() => {
    console.log("veikia shopping scriptas");
    displayProducts();
});

function displayProducts() {
    request = $.ajax({
        method: "POST",
        url: "rest/AdminPanel/GetProducts",
        success: function (allProducts) {
            console.log("success", allProducts);
            $("#productContainer").empty();
            for (i = 0; i < Object.keys(allProducts).length; ++i) {

                let displayProduct = "<div class='product col-3'>";
                displayProduct += "<img src='https://lt2.pigugroup.eu/colours/237/669/45/23766945/d397e8f1f84a9f05642e5a5fe8a9434a_xbig.jpg' width='200' height='200'>";
                displayProduct += "<div class='name'>" + Object.values(allProducts)[i].productInfo[0].name + "</div>";
                displayProduct += "<div class='price'>" + Object.values(allProducts)[i].price + "€</div>";
                displayProduct += "<button type='button' onclick='addProductToShoppingCart(" + Object.values(allProducts)[i].productId + ");'>i krepseli</button>";
                displayProduct += "</div>";
                $("#productContainer").append(displayProduct);
            }


        },
        error: function (error) {
            console.log("error", error);
        }
    });

}

function addProductToShoppingCart(productId) {
    console.log("i krepseli " + productId);

    request = $.ajax({
        method: "POST",
        url: "rest/ShoppingController/AddToShoppingCart/"+productId,
        success: function (response) {
            console.log("gavom success is shoppingCart");
            
        },
        error: function (error) {
            console.log("gavom errora is shopping cart");
        }
    });
}


function shoppingCart(){
    window.location.href ="checkout.html";
}