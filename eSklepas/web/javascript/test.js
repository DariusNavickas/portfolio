/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(()=>{
    checkUserState();
});

function validate() {


    let form = $("#form").children();

    console.log(form.length);


    let validation = true;
    let userName = $("#userName").val();
    let password = $("#password").val();
    let illegalCharacters = RegExp(/([^\w\d]_*)/, 'g');
    for (i = 0; i < form.length; ++i) {
        if ("submit" !== (form[i].id)) {
            console.log(document.getElementById(form[i].id).value);
            let field = document.getElementById(form[i].id);
            if (field.value === null || field.value === "") {
                validation = false;
                $("#alert").text("all fields must be filled");
                break;
            }
            if (illegalCharacters.test(field.value)) {
                $("#alert").text(field.name + " contains illegal characters");
                console.log(field.name + " contains illegal characters");
                console.log(form[i]["name"]);
                validation = false;
                break;

            } else {
                validation = true;
            }
        }
    }
    return validation;
}

function checkUserState() {

    request = $.ajax({
        method: "POST",
        beforeSend: function (request) {
            request.setRequestHeader("Accept", "application/json");
        },
        url: "rest/LoginValidation/GetUserState",
        success: function (response) {
            console.log("User state check success", response);
           

            if (0 == parseInt(response.loggedIn) && 0 == parseInt(response.userType)) {
                console.log("yeaaaa");
                
                $("#admin-button").show();
                //  
            } else {
                console.log("ELSE !!!! : "+ response.loggedIn +"<-lgdIn  "+ response.userType)
                $("#admin-button").hide();
            }

        },
        error: function (error) {
            console.log("No session found", error);

        }
    });
}

function toAdminPage() {
    window.location.href = "adminPanel/adminPage.html";
}


function login() {
    if (validate()) {
        let user = {};
        user.userName = $("#userName").val();
        user.password = $("#password").val();
        request = $.ajax({

            method: "POST",

            beforeSend: function (request) {
                request.setRequestHeader("Content-Type", "application/json");
                request.setRequestHeader("Accept", "text/html");
            },
            url: "rest/LoginValidation/Validate",

            data: JSON.stringify(user),
            processData: true,
            success: function (status) {
                console.log("Entered", status);
                $("#alert").text(status);
                checkUserState();
            },
            error: function (error) {
                console.log("error", error);
                $("#alert").text(error);
            }

        });


    }
}

function toRegister() {
    window.location.href = "register.html";
}

function register() {

    if (validate()) {
        console.log("validation passed");
        let newUser = {};
        newUser.userName = $("#userName").val();
        if ($("#password").val() === $("#rPassword").val()) {
            newUser.password = $("#password").val();
            console.log("passwords match");
        } else {
            console.log("passwords do not match");
            $("#alert").text("passwords do not match");
            return;
        }


        request = $.ajax({

            method: "POST",

            beforeSend: function (request) {
                request.setRequestHeader("Content-Type", "application/json");
                request.setRequestHeader("Accept", "text/html");
            },
            url: "rest/Registration/Register",

            data: JSON.stringify(newUser),
            processData: true,
            success: function (status) {
                console.log("Entered", status);
                $("#alert").text(status);
                //   window.location.href = "index.html";

            },
            error: function (error) {
                console.log("error", error);
                $("#alert").text(error);
            }

        });
    }
}

function logout() {
    request = $.ajax({
        method: "POST",
        url: "rest/LoginValidation/Logout",
        success: function (status) {
            console.log("success", status);
             window.location.href = "index.html";

        },
        error: function (error) {
            console.log("error", error);
        }
    });
}


